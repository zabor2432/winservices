﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace WindowsService
{
    internal static class ConfigureService
    {
        internal static void Configure(string[] args)
        {
            HostFactory.Run(configure =>
            {
                configure.Service<MyService>(service =>
              {
                  service.ConstructUsing(s => new MyService());
                  service.WhenStarted(s => s.onStart(args));
                  service.WhenStopped(s => s.onStop());
              });
                configure.RunAsLocalSystem();
                configure.SetServiceName(Resource1.Name);
                configure.SetDisplayName(Resource1.Name);
                configure.SetDescription(Resource1.Description);
            });
        }
    }
}
