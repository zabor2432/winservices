﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IGenericWindowsService<T>
    {
        Task<T> Start(string[] args);
        Task<T> Stop();
    }

    public interface IGenericWindowsService
    {
        Task Start(string[] args);
        Task Stop();
    }
}
